<!DOCTYPE html>
<html class="no-focus"> <!--<![endif]-->
<?php 
    session_start();
    include "config/config.php";
    if(!isset($_SESSION['username'])) {
    	header("Location:login.php");
	}
?>

    <head>
        <meta charset="utf-8">

        <title>Data Question | DB2 Project</title>

        <?php include('partials/css-data.php') ?>
        <link rel="stylesheet" href="assets/js/plugins/datatables/jquery.dataTables.min.css">
    </head>
    <body>
        <div id="page-container" class="sidebar-l sidebar-o side-scroll header-navbar-fixed">
            <!-- Sidebar -->
            <?php include('partials/sidebarmenu.php'); ?>
            <!-- END Sidebar -->

            <!-- Header -->
            <?php include('partials/header-top.php'); ?>
            <!-- END Header -->

            <!-- Main Container -->
            <main id="main-container">
                <!-- Page Header -->
                <div class="content bg-gray-lighter">
                    <h1 class="page-heading">
                        Data Question <small>Tables</small>
                    </h1>
                    <br>
                    <a href="input-question.php" class="btn btn-sm btn-primary">Input New Question</a>
                </div>
                <!-- END Page Header -->
                <!-- Page Content -->
                <div class="content">
                    <div class="block">
                        <div class="block-content">
                            <!-- DataTables init on table by adding .js-dataTable-simple class, functionality initialized in js/pages/base_tables_datatables.js -->
                            <table class="table table-bordered table-striped js-dataTable-simple">
                                <thead>
                                    <tr>
                                        <th class="text-center" style="width:5%;"></th>
                                        <th style="width: 20%;">Topic</th>
                                        <th class="hidden-xs" style="width:5%;">Difficulty</th>
                                        <th class="hidden-xs">Question</th>
                                        <th class="text-center" style="width: 10%;">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
								<?php
									$no = 1;
									$question = simplexml_load_file('questions.xml');
										echo '<tr>';
										foreach ($question as $question_list):
											$topic=$question_list->topic;
											$question_text=$question_list->question_text;
											$level=$question_list->level;
		                                    echo '<td class="text-center">'.$no.'</td>
											<td class="font-w600">'.$topic.'</td>
		                                    <td class="hidden-xs">
		                                    	<span class="label label-warning">'.$level.'</span>
		                                    </td>
		                                    <td class="hidden-xs">'.$question_text.'</td>
		                                    <td class="text-center">
		                                        <div class="btn-group">
		                                            <button class="btn btn-xs btn-default" type="button" data-toggle="tooltip" title="Edit Client"><i class="fa fa-pencil"></i></button>
		                                            <button class="btn btn-xs btn-default" type="button" data-toggle="tooltip" title="Remove Client"><i class="fa fa-times"></i></button>
		                                        </div>
		                                    </td>
		                                </tr>';
		                                $no++;
	                                endforeach;
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- END Page Content -->
            </main>
            <!-- END Main Container -->

            <!-- Footer -->
            <?php include('partials/footer.php'); ?>
            <!-- END Footer -->
        </div>
        <!-- END Page Container -->
    <!-- Javascript Data -->
    <?php include('partials/js-data.php') ?>
    <!-- Page JS Plugins -->
    <script src="assets/js/plugins/datatables/jquery.dataTables.min.js"></script>
    <!-- Page JS Code -->
    <script src="assets/js/pages/base_tables_datatables.js"></script>
    </body>
</html>