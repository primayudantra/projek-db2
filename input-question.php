<!DOCTYPE html>
<html class="no-focus"> <!--<![endif]-->
	<?php session_start();
    include "config/config.php";
    if(!isset($_SESSION['username'])) {
    	header("Location:login.php");
	}
	?>
    <head>
        <meta charset="utf-8">

        <title>DB2 Project</title>

        <?php include('partials/css-data.php') ?>
    </head>
    <body>
        <div id="page-container" class="sidebar-l sidebar-o side-scroll header-navbar-fixed">
            <!-- Sidebar -->
            <?php include('partials/sidebarmenu.php'); ?>
            <!-- END Sidebar -->

            <!-- Header -->
            <?php include('partials/header-top.php'); ?>
            <!-- END Header -->

            <!-- Main Container -->
            <main id="main-container">
                <!-- Page Content -->
                <div class="content">
                	<div class="block">
                                <div class="block-header">
                                    <ul class="block-options">
                                        <li>
                                        	<a href="data-question.php">Back to Manage Question</a>
                                        </li>
                                    </ul>
                                    <h3 class="block-title">Input Question</h3>
                                </div>
                                <div class="block-content block-content-narrow">
                                    <form class="form-horizontal push-10-t" method="post" action="controller/doinputquestion.php">
                                        <div class="form-group">
                                            <div class="col-sm-9">
                                                <div class="form-material form-material-primary">
                                                    <input class="form-control" type="text" id="material-color-primary" name="topic" placeholder="On focus">
                                                    <label for="material-color-primary">Topic</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <div class="form-material">
                                                    <textarea class="form-control" id="material-textarea-large material-color-success" name="question_text" rows="8" placeholder="Whats your question?"></textarea>
                                                    <label for="material-textarea-large material-color-success">Question</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-9">
                                                <div class="form-material">
                                                    <select class="form-control" id="material-select" name="level" size="1">
                                                        <option>...</option>
                                                        <option value="Easy">Easy</option>
                                                        <option value="Normal">Normal</option>
                                                        <option value="Hard">Hard</option>
                                                    </select>
                                                    <label for="material-select">Please Select level</label>
                                                </div>
                                            </div>
                                        </div>
										<div class="form-group">
                                            <div class="col-sm-9">
                                                <button class="btn btn-sm btn-warning" type="reset">Reset</button>
                                                <button class="btn btn-sm btn-primary" type="submit">Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                </div>
                <!-- END Page Content -->
            </main>
            <!-- END Main Container -->

            <!-- Footer -->
            <?php include('partials/footer.php'); ?>
            <!-- END Footer -->
        </div>
        <!-- END Page Container -->
    <!-- Javascript Data -->
    <?php include('partials/js-data.php') ?>

    </body>
</html>input-questio