<footer id="page-footer" class="content-mini content-mini-full font-s12 bg-gray-lighter clearfix">
    <div class="pull-left">
        <a class="font-w600" href="javascript:void(0)" target="_blank">Project DB2</a> &copy; <span class="js-year-copy"></span>
    </div>
</footer>