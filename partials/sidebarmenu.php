<nav id="sidebar">
    <!-- Sidebar Scroll Container -->
    <div id="sidebar-scroll">
        <!-- Sidebar Content -->
        <!-- Adding .sidebar-mini-hide to an element will hide it when the sidebar is in mini mode -->
        <div class="sidebar-content">
            <!-- Side Header -->
            <div class="side-header side-content bg-white-op">
                <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
                <button class="btn btn-link text-gray pull-right hidden-md hidden-lg" type="button" data-toggle="layout" data-action="sidebar_close">
                    <i class="fa fa-times"></i>
                </button>
                <!-- Themes functionality initialized in App() -> uiHandleTheme() -->
                <a class="h5 text-white" href="index.php">
                    DB2 Project
                </a>
            </div>
            <!-- END Side Header -->

            <!-- Side Content -->
            <div class="side-content">
                <ul class="nav-main">
                    <li>
                        <a href="index.php"><i class="si si-speedometer"></i><span class="sidebar-mini-hide">Dashboard</span></a>
                    </li>
                <?php if($_SESSION['user_id'] == 1){
                    echo '<li class="nav-main-heading"><span class="sidebar-mini-hide">Administration Menu</span></li>
                        <li><a href="data-user.php"><i class="si si-user"></i>Manage Users</span></a></li>
                        <li><a href="data-question.php"><i class="si si-question"></i>Manage Questions</span></a></li>
                        <li><a href="manage-test.php"><i class="si si-notebook"></i>Manage Test</span></a></li>';
                }
                ?>
                <?php
                    if($_SESSION['status'] == "member"){
                    echo'<li class="nav-main-heading"><span class="sidebar-mini-hide">Member Menu</span></li>
                        <li><a href="test-exam.php"><i class="glyphicon glyphicon-list-alt"></i>Write a Test</span></a></li>
                        <li><a href="result-test.php"><i class="fa fa-sort-numeric-asc"></i>Review results</span></a></li>';
                    }
                ?>
                    <li class="nav-main-heading"><span class="sidebar-mini-hide">Log Out</span></li>
                        <li><a href="controller/dologout.php"><i class="si si-logout"></i>Log Out</span></a></li>
                </ul>
            </div>
            <!-- END Side Content -->
        </div>
        <!-- Sidebar Content -->
    </div>
    <!-- END Sidebar Scroll Container -->
</nav>