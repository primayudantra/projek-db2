<!DOCTYPE html>
<html class="no-focus"> <!--<![endif]-->
	<?php 
		session_start();
	    include "config/config.php";
	    if(!isset($_SESSION['username'])) {
	    	header("Location:login.php");
		}
	?>
    <head>
        <meta charset="utf-8">

        <title>DB2 Project</title>

        <?php include('partials/css-data.php') ?>
    </head>
    <body>
        <div id="page-container" class="sidebar-l sidebar-o side-scroll header-navbar-fixed">
            <!-- Sidebar -->
            <?php include('partials/sidebarmenu.php'); ?>
            <!-- END Sidebar -->

            <!-- Header -->
            <?php include('partials/header-top.php'); ?>
            <!-- END Header -->

            <!-- Main Container -->
            <main id="main-container">
                <!-- Page Content -->
                <div class="content">
                	<div class="block">
                                <div class="block-header">
                                    <ul class="block-options">
                                        <li>
                                        	<a href="data-user.php" style="color:blue;">Back to Data User</a>
                                        </li>
                                    </ul>
                                    <h3 class="block-title">Update Password</h3>
                                    <?php if( isset($_GET['msg'])){ ?>
                                		<span style="color:red;"><?php echo $_GET['msg']; ?></span>
                            		<?php } ?>
                                </div>
                                <div class="block-content block-content-narrow">
                                    <form class="form-horizontal push-10-t" method="post" action="controller/doupdatepassword.php">

                                    <?php
									if(isset($_GET["id"])){
										if(empty($_GET["id"])) header('Location:data-user.php');
										$varQuery = "SELECT * FROM users WHERE user_id=".$_GET["id"];
										$query = mysql_query($varQuery);
										$row = mysql_fetch_array($query);
									?>
										<div class="form-group">
                                            <div class="col-sm-9">
                                                <div class="form-material">
                                                    <input class="form-control" type="text" id="material-disabled" disabled placeholder="<?php echo $row['password']; ?>" name="old-password">
                                                    <label for="material-disabled">Old Password</label>
                                                </div>
                                            </div>
                                        </div>                                  
                                        <div class="form-group">
                                            <div class="col-sm-9">
                                                <div class="form-material form-material-primary">
                                                    <input class="form-control" type="text" id="material-color-primary" name="new-password" placeholder="On focus">
                                                    <label for="material-color-primary">New Password</label>
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" value="<?php echo $row['user_id'] ?>" name="user_id">
                                        <input type="hidden" value="<?php echo $row['username'] ?>" name="username">
										<div class="form-group">
                                            <div class="col-sm-9">
                                                <button class="btn btn-sm btn-warning" type="reset">Reset</button>
                                                <button class="btn btn-sm btn-primary" type="submit">Submit</button>
                                            </div>
                                        </div>
                                        <?php } ?>  
                                    </form>
                                </div>
                            </div>
                </div>
                <!-- END Page Content -->
            </main>
            <!-- END Main Container -->

            <!-- Footer -->
            <?php include('partials/footer.php'); ?>
            <!-- END Footer -->
        </div>
        <!-- END Page Container -->
    <!-- Javascript Data -->
    <?php include('partials/js-data.php') ?>

    </body>
</html>