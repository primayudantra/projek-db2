<!DOCTYPE html>
<html class="no-focus"> <!--<![endif]-->
<?php 
    session_start();
    include "config/config.php";
    if(!isset($_SESSION['username'])) {
        header("Location:login.php");
    }
?>
    <head>
        <meta charset="utf-8">
        <title>Data User | DB2 Project</title>
        <?php include('partials/css-data.php') ?>
        <!-- Page JS Plugins CSS -->
        <link rel="stylesheet" href="assets/js/plugins/datatables/jquery.dataTables.min.css">
    </head>
    <body>
        <div id="page-container" class="sidebar-l sidebar-o side-scroll header-navbar-fixed">
            <!-- Sidebar -->
            <?php include('partials/sidebarmenu.php'); ?>
            <!-- END Sidebar -->

            <!-- Header -->
            <?php include('partials/header-top.php'); ?>
            <!-- END Header -->

            <!-- Main Container -->
            <main id="main-container">
                <!-- Page Header -->
                <div class="content bg-gray-lighter">
                    <div class="row items-push">
                        <div class="col-sm-7">
                            <h1 class="page-heading">
                                Data User <small>Page</small>
                            </h1>
                        </div>
                    </div>
                </div>
                <!-- END Page Header -->
                <!-- Page Content -->
                <div class="content">
                                       <!-- Dynamic Table Full -->
                    <div class="block">
                        <div class="block-header">
                            <h3 class="block-title">Data User <small>Table</small></h3>
                            <?php
                                if( isset($_GET['msg'])){ ?>
                                <span style="color:red;"><?php echo $_GET['msg']; ?></span>
                            <?php } ?>
                        </div>
                        <div class="block-content">
                            <!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
                            <table class="table table-bordered table-striped js-dataTable-full">
                                <thead>
                                    <tr>
                                        <th>Fullname</th>
                                        <th>Username</th>
                                        <th class="hidden-xs" style="width: 15%;">Email</th>
                                        <th class="hidden-xs" style="width: 15%;">Password</th>
                                        <th class="text-center" style="width: 15%;">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                    $varQuery = "select user_id,fullname,username,email,password from users" ;
                                    $query    = mysql_query($varQuery);
                                    while($row = mysql_fetch_array($query)){
                                    echo '<tr>
                                        <td class="font-w600">'.$row['fullname'].'</td>
                                        <td class="hidden-xs">'.$row['username'].'</td>
                                        <td class="hidden-xs">'.$row['email'].'</td>
                                        <td class="hidden-xs">'.$row['password'].'</td>
                                        <td class="text-center">
                                            <div class="btn-group">
                                                <a href="update-member.php?id='.$row['user_id'].'" class="btn btn-xs btn-default" type="button" data-toggle="tooltip" title="Update Password"><i class="fa fa-pencil"></i> Update</a>
                                                <form action="controller/dodeleteusers.php" method="post">
                                                    <button class="btn btn-xs btn-default" type="submit" data-toggle="tooltip" title="Remove Client" name="delete" value="delete"><i class="fa fa-times"> Delete</i></button>
                                                        <input type="hidden" name="user_id" value="'.$row['user_id'].'">
                                                        <input type="hidden" name="username" value="'.$row['username'].'">
                                                </form>
                                            </div>
                                        </td>
                                    </tr>';
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END Dynamic Table Full -->

                </div>
                <!-- END Page Content -->
            </main>
            <!-- END Main Container -->

            <!-- Footer -->
            <?php include('partials/footer.php'); ?>
            <!-- END Footer -->
        </div>
        <!-- END Page Container -->
    <!-- Javascript Data -->
    <?php include('partials/js-data.php') ?>
    <!-- Page JS Plugins -->
    <script src="assets/js/plugins/datatables/jquery.dataTables.min.js"></script>
    <!-- Page JS Code -->
    <script src="assets/js/pages/base_tables_datatables.js"></script>

    </body>
</html>