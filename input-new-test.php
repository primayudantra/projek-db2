<!DOCTYPE html>
<html class="no-focus"> <!--<![endif]-->
<?php 
    session_start();
    include "config/config.php";
    if(!isset($_SESSION['username'])) {
    	header("Location:login.php");
	}
?>

    <head>
        <meta charset="utf-8">

        <title>Data Question | DB2 Project</title>

        <?php include('partials/css-data.php') ?>
        <link rel="stylesheet" href="assets/js/plugins/datatables/jquery.dataTables.min.css">
    </head>
    <body>
        <div id="page-container" class="sidebar-l sidebar-o side-scroll header-navbar-fixed">
            <!-- Sidebar -->
            <?php include('partials/sidebarmenu.php'); ?>
            <!-- END Sidebar -->

            <!-- Header -->
            <?php include('partials/header-top.php'); ?>
            <!-- END Header -->

            <!-- Main Container -->
            <main id="main-container">
                <!-- Page Header -->
                <div class="content bg-gray-lighter">
                    <h1 class="page-heading">
                        Input New Test <small>Tables</small>
                    </h1>
                    <br>
                    <a href="manage-test.php" class="btn btn-sm btn-primary">Back to Manage Test</a>
                </div>
                <!-- END Page Header -->
                <!-- Page Content -->
                <div class="content">
                    <div class="block">
                        <div class="block-content">
                            <form action="" method="post">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-10">
                                            <div class="form-material form-material-primary">
                                                <input class="form-control" type="text" id="material-color-primary" name="testName" required="required" placeholder="Test Title">
                                                <label for="material-color-primary">New Test Name</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <button type="reset" class="btn btn-sm btn-danger">Reset</button>
                                            <button type="submit" class="btn btn-sm btn-success">Submit</button>
                                        </div>
                                    </div>
                                </div>
                                 <table class="table table-bordered table-striped js-dataTable-simple">
                                    <thead>
                                        <tr>
                                            <th class="text-center" style="width:5%;"></th>
                                            <th style="width: 20%;">Topic</th>
                                            <th class="hidden-xs" style="width:5%;">Difficulty</th>
                                            <th class="hidden-xs">Question</th>
                                            <th class="text-center" style="width: 10%;">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        $no = 1;
                                        $question = simplexml_load_file('questions.xml');
                                            echo '<tr>';
                                            foreach ($question as $question_list):
                                                $topic=$question_list->topic;
                                                $question_text=$question_list->question_text;
                                                $level=$question_list->level;
                                                echo '<td class="text-center">'.$no.'</td>
                                                <td class="font-w600">'.$topic.'</td>
                                                <td class="hidden-xs">
                                                    <span class="label label-warning">'.$level.'</span>
                                                </td>
                                                <td class="hidden-xs">'.$question_text.'</td>
                                                <td class="text-center">
                                                    <input type="checkbox">
                                                </td>
                                            </tr>';
                                            $no++;
                                        endforeach;
                                    ?>
                                    </tbody>
                                </table>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- END Page Content -->
            </main>
            <!-- END Main Container -->

            <!-- Footer -->
            <?php include('partials/footer.php'); ?>
            <!-- END Footer -->
        </div>
        <!-- END Page Container -->
    <!-- Javascript Data -->
    <?php include('partials/js-data.php') ?>
    <!-- Page JS Plugins -->
    <script src="assets/js/plugins/datatables/jquery.dataTables.min.js"></script>
    <!-- Page JS Code -->
    <script src="assets/js/pages/base_tables_datatables.js"></script>
    </body>
</html>