<!DOCTYPE html>
<html class="no-focus"> <!--<![endif]-->
<?php 
    session_start();
    include "config/config.php";
    if(!isset($_SESSION['username'])) {
    	header("Location:login.php");
	}
?>

    <head>
        <meta charset="utf-8">

        <title>Data Question | DB2 Project</title>

        <?php include('partials/css-data.php') ?>
        <link rel="stylesheet" href="assets/js/plugins/datatables/jquery.dataTables.min.css">
    </head>
    <body>
        <div id="page-container" class="sidebar-l sidebar-o side-scroll header-navbar-fixed">
            <!-- Sidebar -->
            <?php include('partials/sidebarmenu.php'); ?>
            <!-- END Sidebar -->

            <!-- Header -->
            <?php include('partials/header-top.php'); ?>
            <!-- END Header -->

            <!-- Main Container -->
            <main id="main-container">
                <!-- Page Header -->
                <div class="content bg-gray-lighter">
                    <h1 class="page-heading">
                        Examination <small>Title</small>
                    </h1>
                    <br>
                </div>
                <!-- END Page Header -->
                <!-- Page Content -->
                <div class="content">
                    <div class="block">
                        <div class="block-content">
                                <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th class="text-center" style="width: 10%;">#</th>
                                                <th style="width:30%">Title Test</th>
                                                <th class="hidden-xs" style="width: 50%;">Score</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="text-center">1</td>
                                                <td class="font-w600">Test DB2 Configuration</td>
                                                <td class="font-w600">Result : 85</td>
                                        </tbody>
                                    </table>
                        </div>
                    </div>
                </div>
                <!-- END Page Content -->
            </main>
            <!-- END Main Container -->

            <!-- Footer -->
            <?php include('partials/footer.php'); ?>
            <!-- END Footer -->
        </div>
        <!-- END Page Container -->
    <!-- Javascript Data -->
    <?php include('partials/js-data.php') ?>
    <!-- Page JS Plugins -->
    <script src="assets/js/plugins/datatables/jquery.dataTables.min.js"></script>
    <!-- Page JS Code -->
    <script src="assets/js/pages/base_tables_datatables.js"></script>
    </body>
</html>