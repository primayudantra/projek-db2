<?php
	session_start();
	include "../config/config.php";

	$xml = new DOMDocument();
	$xml->load('../questions.xml');

 	// CONFIGURATION XML
	
    $topic = $_POST['topic'];
    $question_text = $_POST['question_text'];
    $level = $_POST['level'];

    $question = $xml->addChild('question');
    $question->addChild('topic', $topic);
    $question->addChild('question_text', $question_text);
    $question->addChild('level', $level);



    // file_put_contents('../questions.xml', $xml->asXML());
    echo $xml->asXML();
    header('Location:../data-question.php');
?>